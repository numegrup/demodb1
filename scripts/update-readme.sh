#!/bin/sh


DIAGRAM_CONTENT=$(PGPASSWORD=$POSTGRES_PASSWORD psql -h "postgres" -U $POSTGRES_USER -d $POSTGRES_DB -A -t -f ./src/main/resources/mermaid-diagram.sql | sed '/^\s*$/d')
DIAGRAM_MARKER="## Database Diagram"
MERMAID_START='```mermaid'
MERMAID_END='```'
TEMP_FILE="temp.md"
NEW_DIAGRAM_CONTENT="$DIAGRAM_MARKER\n$MERMAID_START\n$DIAGRAM_CONTENT\n$MERMAID_END"

if [ ! -f README.md ]; then
  echo -e "$NEW_DIAGRAM_CONTENT" > README.md
else
  if grep -q "$DIAGRAM_MARKER" README.md; then
    awk -v start="$DIAGRAM_MARKER" -v end="$MERMAID_END" -v replacement="$NEW_DIAGRAM_CONTENT" 'BEGIN {p=1} $0 ~ start {print replacement; p=0} $0 ~ end {p=1; next} p {print}' README.md > $TEMP_FILE
  else
    echo -e "\n$NEW_DIAGRAM_CONTENT" >> README.md
    cp README.md $TEMP_FILE
  fi
  mv $TEMP_FILE README.md
fi

cat README.md