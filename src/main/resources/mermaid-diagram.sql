-- Initial setup for Mermaid diagram
select 'erDiagram'
union all
-- Entity definitions with attributes
select
    format(E'\t%s{\n%s\n}',
           c.relname,
           string_agg(format(E'\t\t~%s~ %s',
                             format_type(t.oid, a.atttypmod),
                             a.attname
                      ), E'\n'))
from
    pg_class c
        join pg_namespace n on n.oid = c.relnamespace
        left join pg_attribute a ON c.oid = a.attrelid and a.attnum > 0 and not a.attisdropped
        left join pg_type t ON a.atttypid = t.oid
where
    c.relkind in ('r', 'p')
  and not c.relispartition
  and n.nspname !~ '^pg_' AND n.nspname <> 'information_schema'
group by c.relname
union all
-- Relationships without explicit cardinality
select
    case
        when uc.conname is not null then format(E'%s ||--|| %s : "%s"', c1.relname, c2.relname, c.conname)
        else format(E'%s ||--|{ %s : "%s"', c1.relname, c2.relname, c.conname)
        end
from
    pg_constraint c
        join pg_class c1 on c.conrelid = c1.oid and c.contype = 'f'
        join pg_class c2 on c.confrelid = c2.oid
        left join pg_constraint uc on c.conrelid = uc.conrelid and uc.contype = 'u'
where
    not c1.relispartition and not c2.relispartition;