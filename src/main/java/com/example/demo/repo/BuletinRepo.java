package com.example.demo.repo;

import com.example.demo.model.IdentityCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BuletinRepo extends JpaRepository<IdentityCard, Long> {

}
