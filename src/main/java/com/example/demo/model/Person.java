package com.example.demo.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String surname;
    @OneToOne(mappedBy = "person")
    private IdentityCard identityCard;
    @OneToMany(mappedBy = "person")
    private Set<BlogPost> blogPosts;
    @ManyToMany(mappedBy = "students")
    private Set<Course> courses;
}